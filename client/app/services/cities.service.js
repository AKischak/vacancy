'use strict';

angular.module('vacancyApp').service('CitiesService', [
    function(){
        this.getAll = function(){
            return [
                'London',
                'Berlin',
                'Paris',
                'Rome',
                'Moscow'
            ];
        }
    }
]);