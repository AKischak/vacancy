'use strict';

angular.module('vacancyApp')
    .directive('subscriptionForm', [function(){
        return {
            restrict: "AE",
            controller: 'SubscriptionCtrl',
            scope: {},
            templateUrl: 'app/directives/subscription/subscription.html'
        };
    }]);
