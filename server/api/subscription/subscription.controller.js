/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/subscriptions              ->  index
 * POST    /api/subscriptions              ->  create
 * GET     /api/subscriptions/:id          ->  show
 * PUT     /api/subscriptions/:id          ->  update
 * DELETE  /api/subscriptions/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Subscription from './subscription.model';
import Vacancy from './../vacancy/vacancy.model';
import config from './../../config/environment';

var domain = 'slesn.info';
var mailgun = require('mailgun-js')({ apiKey: config['mailgun']['apiKey'], domain: domain });

function respondWithResult(res, statusCode) {
    statusCode = statusCode || 200;
    return function(entity) {
        if (entity) {
            res.status(statusCode).json(entity);
        }
    };
}

function saveUpdates(updates) {
    return function(entity) {
        var updated = _.merge(entity, updates);
        return updated.saveAsync()
            .spread(updated => {
                return updated;
            });
    };
}

function removeEntity(res) {
    return function(entity) {
        if (entity) {
            return entity.removeAsync()
                .then(() => {
                    res.status(204).end();
                });
        }
    };
}

function handleEntityNotFound(res) {
    return function(entity) {
        if (!entity) {
            res.status(404).end();
            return null;
        }
        return entity;
    };
}

function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function(err) {
        res.status(statusCode).send(err);
    };
}

// Gets a list of Subscriptions
export function index(req, res) {
    Subscription.findAsync()
        .then(respondWithResult(res))
        .catch(handleError(res));
}

// Gets a single Subscription from the DB
export function show(req, res) {
    Subscription.findByIdAsync(req.params.id)
        .then(handleEntityNotFound(res))
        .then(respondWithResult(res))
        .catch(handleError(res));
}

// Creates a new Subscription in the DB
export function create(req, res) {
    Subscription.createAsync(req.body)
        .then(respondWithResult(res, 201))
        .catch(handleError(res));
}

// Updates an existing Subscription in the DB
export function update(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Subscription.findByIdAsync(req.params.id)
        .then(handleEntityNotFound(res))
        .then(saveUpdates(req.body))
        .then(respondWithResult(res))
        .catch(handleError(res));
}

// Deletes a Subscription from the DB
export function destroy(req, res) {
    Subscription.findByIdAsync(req.params.id)
        .then(handleEntityNotFound(res))
        .then(removeEntity(res))
        .catch(handleError(res));
}


export function send(req, res){
    Subscription.findByIdAsync(req.params.id)
        .then(handleEntityNotFound(res))
        .then(function(entity) {
            //var oldQuery = { city: entity.city, category: { $in: entity.categories }, "salaryType.title": entity.salaryRange.title };
            var query = { city: entity.city, category: { $in: entity.categories }, salaryType: entity.salaryRange.id };

            return Vacancy.findAsync(query).then((data) => {
                let promises = [];
                _.each(data, (vacancy) => {
                    var data = {
                        from: '#COMPANY# <#EMAIL#>'.replace('#COMPANY#', vacancy.company).replace('#EMAIL#', vacancy.email),
                        to: entity.email,
                        subject: "title [city][salary][company]".replace('title', vacancy.title).replace('city', vacancy.city).replace('salary', vacancy.salaryRange.title).replace('company', vacancy.company),
                        html: vacancy.description
                    };

                    let pr = new Promise((resolve, reject) => {                        
                        mailgun.messages().send(data, function (error, body) {
                            if(error){
                                console.log(error);
                                reject();
                            }
                            resolve();
                        });
                    });
                    promises.push(pr);
                });

                return Promise.all(promises).then(() => {
                    return { send: true };
                }).catch(() => {
                    return { send: false }
                });
            });
        })
        .then(respondWithResult(res, 200))
        .catch(handleError(res));
}