'use strict';

angular.module('vacancyApp')
  .controller('AdminCtrl', [
      '$scope',
      '$http',
      'toaster',
      'SalaryService',
  function ($scope, $http, toaster, SalaryService) {
      $http({
          method: 'GET',
          url: '/api/vacancy'
      }).then(function(response){
          $scope.vacancyList = response.data;
      });


      $http({
          method: 'GET',
          url: '/api/subscriptions'
      }).then(function(response){
          $scope.subscriptionsList = response.data;
      });

      $scope.removeVacancy = function(item){
          $http({
              method: 'DELETE',
              url: '/api/vacancy/' + item._id
          }).then(function(response){
              $scope.vacancyList = _.filter($scope.vacancyList, function(x){ return x._id != item._id; });
          });
      };

      $scope.removeSubscription = function(item){
          $http({
              method: 'DELETE',
              url: '/api/subscriptions/' + item._id
          }).then(function(response){
              $scope.subscriptionsList = _.filter($scope.subscriptionsList, function(x){ return x._id != item._id; });
          });
      };

      $scope.sendNotification = function(item){
          $http({
              method: 'POST',
              url: '/api/subscriptions/send/'+item._id
          }).then(function(response){
              toaster.pop('success', "Notification sent");
          }, function() {
              toaster.pop('error', "Error");
          });
      };

      $scope.getSalaryTitle = function(type){
        var res = SalaryService.getById(type);
        return res ? res.title : "";
      }

  }]);
