'use strict';

angular.module('vacancyApp')
  .directive('distributeForm', [function(){
      return {
          restrict: "AE",
          controller: 'DistributeCtrl',
          scope: {},
          templateUrl: 'app/directives/distribute/distribute.html'
      };
  }]);
