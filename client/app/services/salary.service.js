'use strict';

angular.module('vacancyApp').service('SalaryService', [
    function(){
        var list = [
            {
                id: 1,
                title:  'up to 200 000',
                from: 0,
                to: 200000
            },
            {
                id: 2,
                title: '201 000 - 400 000',
                from: 201000,
                to: 400000
            },
            {
                id: 3,
                title:  '401 000 - 600 000',
                from: 401000,
                to: 600000
            }
        ];


        this.getById = function(id){
            return _.find(list, function(x){
                return x.id == id;
            });
        };

        this.getAll = function(){
            return list;
        }
    }
]);