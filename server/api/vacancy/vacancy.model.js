'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var VacancySchema = new mongoose.Schema({
  title: String,
  company: String,
  description: String,
  city: String,
  category: String,
  salaryType: Number,
  salaryRange: { id: Number, title: String, from: Number, to: Number },
  email: String
});

export default mongoose.model('Vacancy', VacancySchema);
