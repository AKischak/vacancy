'use strict';

angular.module('vacancyApp')
    .controller('DistributeVacancyModalCtrl', ['$scope', '$uibModalInstance', 'salaryRange', function($scope, $uibModalInstance, salaryRange){
        $scope.model = {};
        $scope.salaryRange = salaryRange;
        $scope.dismiss = function(){
            $uibModalInstance.dismiss();
        };

        $scope.saveChanges = function(){
            $uibModalInstance.close($scope.model);
        }
    }])
    .controller('DistributeCtrl', [
        '$scope',
        '$uibModal',
        'CategoriesService',
        'CitiesService',
        'SalaryService',
        '$http',
        function ($scope, $uibModal, CategoriesService, CitiesService, SalaryService, $http) {
            $scope.model = {
                category: null,
                city: null,
                salaryRange: null
            };
            $scope.categoryList = CategoriesService.getAll();
            $scope.cityList = CitiesService.getAll();
            $scope.salaryList = SalaryService.getAll();


            $scope.submit = function(){
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/directives/distribute/modal.template.html',
                    controller: 'DistributeVacancyModalCtrl',
                    controllerAs: '$ctrl',
                    size: 'lg',
                    backdrop: 'static',
                    windowClass: 'custom-modal',
                    backdropClass: 'custom-backdrop',
                    resolve: {
                        salaryRange: function () {
                            return $scope.model.salaryRange;
                        }
                    }
                });

                modalInstance.result.then(function (data) {
                    var params = _.merge({}, $scope.model, data);
                    params.salaryType = params.salaryRange.id;
                    console.log(params);
                    $http({
                        method: 'POST',
                        url: '/api/vacancy',
                        data: params
                    }).then((data) => {
                        $scope.model = {
                            category: null,
                            city: null,
                            salaryRange: null
                        };
                    });
                }, function () {
                    console.log("DISMISS");
                });
        };

    }]);
