'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var SubscriptionSchema = new mongoose.Schema({
  categories: [ String ],
  city: String,
  salary: String,
  email: String,
  salaryRange: { id: Number, title: String, from: Number, to: Number }
});

export default mongoose.model('Subscription', SubscriptionSchema);
