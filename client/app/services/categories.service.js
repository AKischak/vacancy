'use strict';

angular.module('vacancyApp').service('CategoriesService', [
    function(){
        this.getAll = function(){
            return [
                'Civil Engineering',
                'Programming',
                'Public Sector',
                'Economics',
                'Teaching'
            ];
        }
    }
]);