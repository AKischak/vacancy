'use strict';

(function() {

class MainController {

  constructor($http) {
    this.$http = $http;
  }

}

angular.module('vacancyApp')
  .controller('MainController', MainController);

})();
