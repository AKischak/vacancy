(function(angular, undefined) {
'use strict';

angular.module('vacancyApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);