'use strict';

angular.module('vacancyApp')
    .controller('SubscriptionCtrl', ['$scope', 'CategoriesService',
        'CitiesService',
        'SalaryService',
        '$http',
        function ($scope, CategoriesService, CitiesService, SalaryService, $http) {

        $scope.selectedCategories = "";

        $scope.categoryList = CategoriesService.getAll();
        $scope.cityList = CitiesService.getAll();
        $scope.salaryList = SalaryService.getAll();

        $scope.toggleCategorySelected = function(category){
            var index = $scope.model.categories.indexOf(category);

            if(index >= 0){
                $scope.model.categories.splice(index, 1);
            }else{
                if($scope.model.categories.length < 3){
                    $scope.model.categories.push(category);
                }
            }

            $scope.refreshViewSelectedCategories();
        };

        $scope.refreshViewSelectedCategories = function(){
            $scope.selectedCategories = $scope.model.categories ? $scope.model.categories.join(', ') : "";
        };

        $scope.submit = function(){
            $http({
                method: 'POST',
                url: '/api/subscriptions',
                data: $scope.model
            }).then((result) => {
                $scope.model = {
                    categories: [],
                    salaryRange: null,
                    city: null,
                    email: null
                };
                $scope.refreshViewSelectedCategories();
            });
        };

        $scope.model = {
            categories: [],
            salaryRange: null,
            city: null,
            email: null
        };

    }]);
